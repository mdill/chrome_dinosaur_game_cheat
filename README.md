# Arduino Selectable Relay

## Purpose

This Python script is designed to play/win the T-Rex Run Chrome dinosaur game.
By watching various pixels on the screen, it is possible to know when to duck or
jump under/over obstacles.  Appropriate keystrokes are provided to the system
depending on which obstacle is ahead as well as if the game has entered "day" or
"night" mode.

## Download

    cd ~/
    git clone https://bitbucket.org/mdill/chrome_dinosaur_game_cheat.git

## License

This project is licensed under the BSD License - see the [LICENSE.md](https://bitbucket.org/mdill/chrome_dinosaur_game_cheat/src/master/LICENSE.txt) file for
details.

